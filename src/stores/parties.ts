import { browser } from "$app/environment";
import { derived, writable, type Readable } from 'svelte/store';

export interface BaseJoueur {
    id_: number;
    nom: string | null;
}

interface JoueurSansFonction extends BaseJoueur {
    points: number[];
    partie_id: number;
}

export interface Joueur extends JoueurSansFonction {
    ajouterPoints: (points: number) => void;
    supprimerPoint: (index: number) => void;
    editerPoints: (index: number, points: number) => void;
}

interface Joueurs {
    [id: number]: JoueurSansFonction;
}

export interface Partie {
    id_: number;
    joueurs: Joueurs;
    jeu: string | null;
}

interface Parties {
    [id: number]: Partie;
}


export const parties = writable<Parties>({});


function ajouterPoints(joueur: JoueurSansFonction, points:number) {
    parties.update((parties) => {
        if (joueur.partie_id && parties[joueur.partie_id].joueurs[joueur.id_]) {
            parties[joueur.partie_id].joueurs[joueur.id_].points?.push(points)
        }
        return parties
    });
}


function supprimerPoint(joueur: JoueurSansFonction, index: number) {
    parties.update((parties) => {
        if (joueur.partie_id && parties[joueur.partie_id].joueurs[joueur.id_]) {
            parties[joueur.partie_id].joueurs[joueur.id_].points?.splice(index, 1)
        }
        return parties;
    });
}



function editerPoint(joueur: JoueurSansFonction, index: number, points: number) {
    parties.update((parties) => {
        if (joueur.partie_id && parties[joueur.partie_id].joueurs[joueur.id_]) {
            parties[joueur.partie_id].joueurs[joueur.id_].points?.splice(index, 1, points);
        }
        return parties;
    });
}

export function ajouterPartie(base_joueurs: BaseJoueur[], jeu: string | null = null) {
    let partie_id = 1;
    parties.update((parties) => {
        if (Object.keys(parties).length > 0) {
            partie_id = Math.max(...Object.keys(parties).map((p_id) => parseInt(p_id))) + 1;
        }
        let partie: Partie = {id_: partie_id, joueurs: {}, jeu: jeu};
        base_joueurs.map((j, i) => {
            partie.joueurs[j.id_] = {
                ...j,
                nom: j.nom || `Joueur ${i + 1}`,
                points: [],
                partie_id: partie.id_
            };
        });
        parties[partie_id] = partie;
        return parties;
    });
    return partie_id;
}

export function getPartie(partie_id: number | null) {
    return derived(parties, (parties) => {
        if (partie_id === null) throw new Error("Impossible de récupérer la partie")    
        return parties[partie_id];
    })
}

export function dupliquerPartie(partie: Partie) {
    let joueurs: BaseJoueur[] = [];
    Object.values(partie.joueurs).forEach((joueur: BaseJoueur) => {
      joueurs.push({ ...joueur });
    });
    let new_partie_id = ajouterPartie(joueurs);
    return new_partie_id;
}


export function getJoueurStore(base_joueur: JoueurSansFonction) {
    return derived(parties, ($parties) => {
        if (base_joueur.partie_id === undefined || base_joueur.partie_id === null) throw new Error("Etat cassé, pas de partie sur un joueur");
        let j = $parties[base_joueur.partie_id].joueurs[base_joueur.id_];
        if (!j) throw new Error(`Etat cassé, impossible de trouver le joueur ${base_joueur.id_} pour la partie ${base_joueur.partie_id}`)
        let joueur: Joueur = {
            ...j,
            ajouterPoints: (points: number) => ajouterPoints(j, points),
            supprimerPoint:(index: number) => supprimerPoint(j, index),
            editerPoints: (index: number, points: number) => editerPoint(j, index, points)
        }
        return joueur;
    });
}

if (browser) {
    const storedParties = localStorage.getItem('parties');
    if (storedParties) {
        let jsonParties: Parties = JSON.parse(storedParties);
        parties.set(jsonParties);
    }
}

parties.subscribe((parties) => {
    console.log("update parties")
    localStorage.setItem('parties', JSON.stringify(parties));
});